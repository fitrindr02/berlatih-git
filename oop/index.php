<?php

    require('animal.php');
    require('frog.php');
    require('ape.php');

    $sheep = new Animal("shaun");

    echo "Name : $sheep->name <br>"; // "shaun"
    echo "legs : $sheep->legs <br>"; // 4
    echo "cool blooded : $sheep->cold_blooded <br><br>"; // "no"

    $kodok = new Frog("buduk");

    echo "Name : $kodok->name <br>"; 
    echo "legs : $kodok->legs <br>"; 
    echo "cool blooded : $kodok->cold_blooded <br> Jump : "; 
    $kodok->jump();
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");

    echo "Name : $sungokong->name <br>"; 
    echo "legs : $sungokong->legs <br>"; 
    echo "cool blooded : $sungokong->cold_blooded <br> Yell : "; 
    $sungokong->yell();
?>